jQuery(document).ready(function($){
	function emailIsValid (email) {
		return /\S+@\S+\.\S+/.test(email)
	}
	
	$('input[type="tel"]').mask("+7 (999) 999-9999");
	
	$('.etap-next').on('click', function(){
		var err = 0;
		$('.etap__first input,.etap__first textarea').not('.comments, input[type="file"]').each(function(){
			if($(this).val() === ''){
				$(this).addClass('jl-form-danger');
				err++;
			}else{
				$(this).removeClass('jl-form-danger');
			}
		});
		if(err < 1){
			$('.etap1').hide();
			$('.etap2').show();
		}
	});
	
	$('.submit__back').on('click', function(){
		$('.etap1').show();
		$('.etap2').hide();
	});	
	
	/* File */
	var maxFileSize = 5 * 1500 * 1500;
	var queueEst = {};
	var queueAskl = {};
	var imagesList = $('.uploadImagesList');
 
	var itemPreviewTemplate = imagesList.find('.item.template').clone();
	itemPreviewTemplate.removeClass('template');
	imagesList.find('.item.template').remove();
 
 
	$('.addImages').on('change', function () {
		var idFl = $(this).attr('data-list');
		var choose = $(this).attr('data-choose');
		if( $(this)[0].files.length > 10) {
			alert("Не более 10 файлов");
		}else{
			var files = this.files;
			var countFile = 0;
 
			for (var i = 0; i < files.length; i++) {
				var file = files[i];
				countFile++;

				if ( file.size > maxFileSize ) {
					alert( 'Размер фотографии не должен превышать 2 Мб' );
					continue;
				}
				
				if ( !file.type.match(/image\/(jpeg|jpg|png|gif)/) ) {
				   //previewFl(files[i]);
				}else{
					
				}
				preview(files[i],idFl,choose);
				
			}
			this.value = '';

			if( countFile > 0){
				$(this).next().fadeTo(10,0);
			}else{
				$(this).next().fadeTo(10,1);
			}
		}
	});
 
	// Создание превью
	function preview(file,id,chs) {
		var reader = new FileReader();
		reader.addEventListener('load', function(event) {
			var img = document.createElement('img');
 
			var itemPreview = itemPreviewTemplate.clone();
			
			if ( !file.type.match(/image\/(jpeg|jpg|png|gif)/) ) {
				itemPreview.find('.img-wrap img').attr('src', 'forms/img/doc.png');
			}else{
				itemPreview.find('.img-wrap img').attr('src', event.target.result);
			}
			
			itemPreview.find('.delete-link').attr('data-list', id).attr('data-choose', chs);
			itemPreview.find('.img-name').html(file.name);
			itemPreview.data('id', file.name);
			$('#'+id).append(itemPreview);
			if(id === 'upl-est'){
				queueEst[file.name] = file;
			}else{
				queueAskl[file.name] = file;
			}
			
		});
		reader.readAsDataURL(file);
	}
 
	// Удаление фотографий
	imagesList.on('click', '.delete-link', function () {
		var item = $(this).closest('.item'),
			list = $(this).attr('data-list'),
			deleteChoose = $(this).attr('data-choose'),
			id = item.data('id');
		if(list === 'upl-est'){
			delete queueEst[id];
		}else{
			delete queueAskl[id];
		}
		item.remove();
		if($('#'+list+' .item').length < 1){
			$('#'+deleteChoose+' span').last().fadeTo(10,1);
		}
	});
 
 
	// Отправка формы
	var submitUrl = '/forms/submit.php?uploadfiles';
	
	$('#etap').on('submit', function(event) {
		var form = $(this);
		var formData = new FormData(this);
		var countImg = 0;
		for (var id in queueEst) {
			formData.append(countImg, queueEst[id]);
			countImg++;
		}
		formData.append("txtname", $(this).find('input[name="txtname"]').val());
		
		var err = 0;
		$('.etap__last input').not('input[type="submit"]').each(function(){
			if($(this).val() === ''){
				$(this).addClass('jl-form-danger');
				err++;
			}else{
				$(this).removeClass('jl-form-danger');
			}
		});
		
		if(emailIsValid($('.etap__last input[type="email"]').val())){
			$('.etap__last input[type="email"]').removeClass('jl-form-danger');
		}else{
			$('.etap__last input[type="email"]').addClass('jl-form-danger');
			err++;
		}
		
		if(err < 1){
			$.ajax({
				url: submitUrl,
				type: 'POST',
				data: formData,
				async: true,
				success: function( msg ){
					if(msg === 'ok'){
						jlUIkit.modal('#form-est').hide();
						$(':input','#etap').not(':button, :submit, :reset, :hidden').val('');
						jlUIkit.modal('#thanks').show();
						queueEst = {};
						$(form).find('.uploadImagesList .item').remove();
						$('.submit__back').click();
					}else{
						alert('произошла ошибка');
					}
				},
				cache: false,
				processData: false,
				contentType: false,
			});
		}
		
		return false;
	});
	
	/* Form ASKL */
	$('.askl').on('submit', function(event) {
		var form = $(this);
		var formData = new FormData(this);
		var countImg = 0;
		
		for (var id in queueAskl) {
			formData.append(countImg, queueAskl[id]);
			countImg++;
		}
		
		var err = 0;
		$(this).find('.req').each(function(){
			if($(this).val() === ''){
				$(this).addClass('jl-form-danger');
				err++;
			}else{
				$(this).removeClass('jl-form-danger');
			}
		});
		
		if($(this).find('input[type="email"]').val() != ''){
			if(emailIsValid($(this).find('input[type="email"]').val())){
				$(this).find('input[type="email"]').removeClass('jl-form-danger');
			}else{
				$(this).find('input[type="email"]').addClass('jl-form-danger');
				err++;
			}
		}
		
		if(err < 1){
			$.ajax({
				url: submitUrl,
				type: 'POST',
				data: formData,
				async: true,
				success: function( msg ){
					if(msg === 'ok'){
						//jlUIkit.modal('#form-ask').hide();
						$(':input', '#form-ask').not(':button, :submit, :reset, :hidden').val('');
						jlUIkit.modal('#thanks').show();
						queueAskl = {};
						$(form).find('.uploadImagesList .item').remove();
					}else{
						alert('произошла ошибка');
					}
				},
				cache: false,
				processData: false,
				contentType: false,
			});
		}
		
		return false;
	});
	
	/* Call Form */

	var $timeBlock = $('.form-row__time');
	var format = 'HH : mm';
	var time = moment();
	
	var minHours = 10;
	var maxHours = 18;

	time.minutes(Math.ceil(time.minutes() / 15) * 15);
	//time.add('15', 'm');

	if(time.hours() > maxHours || time.hours() < minHours) {
		time = moment(minHours+':00', 'HH:mm');
	}

	$('.time-input').find('input').val(time.format(format));

	$('.call input[type="radio"]').on('change', function() {
		$('.call input[type="radio"]:checked').val() == 'now' ? $timeBlock.slideDown() : $timeBlock.slideUp();
	})

	$('.controls__item').on('click', function(e) {
		e.preventDefault();

		var $input = $(this).closest('.time-input').find('input');

		var time = moment($input.val(), format);

		if($(this).hasClass('controls__item_inc')) {
			if(time.hours() == maxHours) {
				time.hours(minHours);
				time.minutes(0);
			} else {
				if($(this).parent().hasClass('controls-hours')) {
					time.add('1', 'H');
				} else {
					time.add('15', 'm');
				}
			}

			time = moment.min(time, moment(maxHours+':00', 'HH:mm'));
		} else {
			if(time.hours() == minHours && time.minutes() == 0) {
				time.hours(maxHours);
				time.minutes(0);
			} else {
				if($(this).parent().hasClass('controls-hours')) {
					time.subtract('1', 'H');
				} else {
					time.subtract('15', 'm');
				}
			}

			time = moment.max(time, moment(minHours+':00', 'HH:mm'));
		}

		$input.val(time.format(format));
	});
	
	$('.call').on('submit', function(event) {
		
		var form = $(this);
		var formData = new FormData(this);
		
		formData.append("timeCall", $(this).find('input[name="time"]').val());
		
		var err = 0;
		$(this).find('.req').each(function(){
			if($(this).val() === ''){
				$(this).addClass('jl-form-danger');
				err++;
			}else{
				$(this).removeClass('jl-form-danger');
			}
		});
		
		if(err < 1){
			$.ajax({
				url: submitUrl,
				type: 'POST',
				data: formData,
				async: true,
				success: function( msg ){
					if(msg === 'ok'){
						jlUIkit.modal('#form-call').hide();
						$(form).find('.req').val('');
						jlUIkit.modal('#thanks').show();
					}else{
						alert('произошла ошибка');
					}
				},
				cache: false,
				processData: false,
				contentType: false,
			});
		}
		
		return false;
	});
	
});