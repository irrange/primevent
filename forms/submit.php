﻿<?php
 
// Здесь нужно сделать все проверки передаваемых файлов и вывести ошибки если нужно
 
// Переменная ответа
 
$data = array();
 
if( isset( $_GET['uploadfiles'] ) && ( $_POST['formAsk'] == '' )){
    $error = false;
    $files = array();
 
    $uploaddir = './upload/'; // . - текущая папка где находится submit.php
 
    // Создадим папку если её нет
 
    if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );
 
    // переместим файлы из временной директории в указанную
    foreach( $_FILES as $file ){
        if( move_uploaded_file( $file['tmp_name'], $uploaddir . basename($file['name']) ) ){
            $files[] = realpath( $uploaddir . $file['name'] );
        }
        else{
            $error = true;
        }
    }

    // указать email Получателей, можно через запятую
	$to = 'u.oksana@mail.ru,irrange@yandex.ru';
    $from = 'mail@'.$_SERVER['SERVER_NAME'];
    $subject ="Индивидуальный расчет стоимости вентиляции";
	if($_POST['formName'] != ''){
		$subject = $_POST['formName'];
	}
	$subject = "=?utf-8?b?". base64_encode($subject) ."?=";

	$message = "";
	if($_POST['txtname'] && ($_POST['txtname'] != '')){
		$message .= "<br>ФИО: ".$_POST['txtname'];
	}
	if($_POST['email'] && ($_POST['email'] != '')){
		$message .= "<br>email: ".$_POST['email'];
	}
	if($_POST['phone'] && ($_POST['phone'] != '')){
		$message .= "<br>Телефон: ".$_POST['phone'];
	}
	if($_POST['message'] && ($_POST['message'] != '')){
		$message .= "<br>Назначение помещения: ".$_POST['message'];
	}
	if($_POST['area'] && ($_POST['area'] != '')){
		$message .= "<br>Площадь: ".$_POST['area'];
	}
	if($_POST['height'] && ($_POST['height'] != '')){
		$message .= "<br>Высота: ".$_POST['height'];
	}
	if($_POST['nagrev'] && ($_POST['nagrev'] != '')){
		$message .= "<br>Тип нагрева: ".$_POST['nagrev'];
	}
	if($_POST['system'] && ($_POST['system'] != '')){
		$message .= "<br>Система кондиционирования: да";
	}
	if($_POST['comments'] && ($_POST['comments'] != '')){
		$message .= "<br>Комментарии: ".$_POST['comments'];
	}
	
	if($_POST['now'] && ($_POST['now'] == 'now')){
		$message .= "<br>Позвонить в: ".$_POST['timeCall'];
	}
    
    $headers = "From: $from";

    // boundary 
    $semi_rand = md5(time()); 
    $mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 

    // headers for attachment 
    $headers .= "\nMIME-Version: 1.0\n" . "Content-Type: multipart/mixed;\n" . " boundary=\"{$mime_boundary}\""; 

    // multipart boundary 
    $message = "This is a multi-part message in MIME format.\n\n" . "--{$mime_boundary}\n" . "Content-Type: text/html; charset=\"UTF-8\"\n" . "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n"; 
    $message .= "--{$mime_boundary}\n";

    // preparing attachments
    for($x=0;$x<count($files);$x++){
        $file = fopen($files[$x],"rb");
        $data = fread($file,filesize($files[$x]));
        fclose($file);
        $data = chunk_split(base64_encode($data));
        $message .= "Content-Type: {\"application/octet-stream\"};\n" . " name=\"$files[$x]\"\n" . 
        "Content-Disposition: attachment;\n" . " filename=\"$files[$x]\"\n" . 
        "Content-Transfer-Encoding: base64\n\n" . $data . "\n\n";
        $message .= "--{$mime_boundary}\n";
    }

    // send

    $ok = @mail($to, $subject, $message, $headers); 
	if ($ok) {
        echo "ok"; 
    } else { 
        echo "false"; 
    } 
}
?>