<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_login
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JLoader::register('UsersHelperRoute', JPATH_SITE . '/components/com_users/helpers/route.php');

JHtml::_('behavior.keepalive');
JHtml::_('bootstrap.tooltip');

?>
<form action="<?php echo JRoute::_('index.php', true, $params->get('usesecure', 0)); ?>" method="post" id="jl-login-form">
<fieldset class="jl-fieldset">
	<?php if ($params->get('pretext')) : ?>
		<div class="pretext">
			<p><?php echo $params->get('pretext'); ?></p>
		</div>
	<?php endif; ?>
	<div class="jl-width-1-1">
		<div id="form-login-username" class="control-group">
			<div class="controls">
				<?php if (!$params->get('usetext', 0)) : ?>

					<div class="jl-margin">
					<div class="jl-inline jl-width-1-1">
						<span class="jl-form-icon jl-form-icon-flip" jl-icon="icon: user"></span>
						<input id="modlgn-username" class="jl-input" tabindex="0" size="18" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?>" name="username" type="text">
					</div>
				</div>



				<?php else : ?>
					<label for="modlgn-username"><?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?></label>
					<input id="modlgn-username" type="text" name="username" class="input-small" tabindex="0" size="18" placeholder="<?php echo JText::_('MOD_LOGIN_VALUE_USERNAME'); ?>" />
				<?php endif; ?>
			</div>
		</div>
		<div id="form-login-password" class="control-group">
			<div class="controls">
				<?php if (!$params->get('usetext', 0)) : ?>

					<div class="jl-margin">
					<div class="jl-inline jl-width-1-1">
						<span class="jl-form-icon jl-form-icon-flip" jl-icon="icon: lock"></span>
						<input id="modlgn-passwd" type="password" name="password" class="jl-input" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" >
					</div>
				</div>

				<?php else : ?>
					<label for="modlgn-passwd"><?php echo JText::_('JGLOBAL_PASSWORD'); ?></label>
					<input id="modlgn-passwd" type="password" name="password" class="input-small" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_PASSWORD'); ?>" />
				<?php endif; ?>
			</div>
		</div>
		<?php if (count($twofactormethods) > 1) : ?>
		<div id="form-login-secretkey" class="control-group">
			<div class="controls">
				<?php if (!$params->get('usetext', 0)) : ?>
					<div class="jl-inline jl-width-1-1">
						<div class="controls">
						<span class="jl-form-icon jl-form-icon-flip" jl-icon="icon: question"></span>
						<input id="modlgn-secretkey" autocomplete="off" type="text" name="secretkey" class="jl-width-1-1" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" jl-tooltip="<?php echo JText::_('JGLOBAL_SECRETKEY_HELP'); ?>" />
						</div>
					</div>
				<?php else : ?>
					<label for="modlgn-secretkey"><?php echo JText::_('JGLOBAL_SECRETKEY'); ?></label>
					<input id="modlgn-secretkey" autocomplete="off" type="text" name="secretkey" class="input-small" tabindex="0" size="18" placeholder="<?php echo JText::_('JGLOBAL_SECRETKEY'); ?>" />
					<span class="btn width-auto hasTooltip" title="<?php echo JText::_('JGLOBAL_SECRETKEY_HELP'); ?>">
						<span class="icon-help"></span>
					</span>
				<?php endif; ?>

			</div>
		</div>
		<?php endif; ?>

		<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>

			<div class="jl-margin">
				<label for="modlgn-remember"><input id="modlgn-remember" class="jl-checkbox" type="checkbox" name="remember" class="inputbox" value="yes"> <?php echo JText::_('MOD_LOGIN_REMEMBER_ME'); ?></label>
			</div>

		<?php endif; ?>

		<div id="form-login-submit" class="control-group">
			<div class="controls">
				<button type="submit" tabindex="0" name="Submit" class="jl-button jl-button-primary jl-width-1-1"><?php echo JText::_('JLOGIN'); ?></button>
			</div>
		</div>
		<?php
			$usersConfig = JComponentHelper::getParams('com_users'); ?>
			<ul class="jl-list jl-text-center">
			<?php if ($usersConfig->get('allowUserRegistration')) : ?>
				<li>
					<a class="jl-link-reset jl-text-small" href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
					<?php echo JText::_('MOD_LOGIN_REGISTER'); ?> <span class="jl-icon" jl-icon="arrow-right"></span></a>
				</li>
			<?php endif; ?>
				<li>
					<a class="jl-link-reset jl-text-small" href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
					<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_USERNAME'); ?></a>
				</li>
				<li>
					<a class="jl-link-reset jl-text-small" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
					<?php echo JText::_('MOD_LOGIN_FORGOT_YOUR_PASSWORD'); ?></a>
				</li>
			</ul>
		<input type="hidden" name="option" value="com_users" />
		<input type="hidden" name="task" value="user.login" />
		<input type="hidden" name="return" value="<?php echo $return; ?>" />
		<?php echo JHtml::_('form.token'); ?>
	</div>
	<?php if ($params->get('posttext')) : ?>
		<div class="posttext">
			<p><?php echo $params->get('posttext'); ?></p>
		</div>
	<?php endif; ?>
	</fieldset>
</form>
