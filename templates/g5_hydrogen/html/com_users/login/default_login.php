<?php
/**
 * @package     Joomla.Site
 * @subpackage  com_users
 *
 * @copyright   Copyright (C) 2005 - 2018 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

JHtml::_('behavior.keepalive');
JHtml::_('behavior.formvalidator');

?>
<div class="tm-form-login jl-flex jl-flex-center jl-flex-middle<?php echo $this->pageclass_sfx; ?>">
	<?php if ($this->params->get('show_page_heading')) : ?>
		<div class="page-header">
			<h1>
				<?php echo $this->escape($this->params->get('page_heading')); ?>
			</h1>
		</div>
	<?php endif; ?>
	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
		<div class="login-description">
	<?php endif; ?>
	<?php if ($this->params->get('logindescription_show') == 1) : ?>
		<?php echo $this->params->get('login_description'); ?>
	<?php endif; ?>
	<?php if ($this->params->get('login_image') != '') : ?>
		<img src="<?php echo $this->escape($this->params->get('login_image')); ?>" class="login-image" alt="<?php echo JText::_('COM_USERS_LOGIN_IMAGE_ALT'); ?>" />
	<?php endif; ?>
	<?php if (($this->params->get('logindescription_show') == 1 && str_replace(' ', '', $this->params->get('login_description')) != '') || $this->params->get('login_image') != '') : ?>
		</div>
	<?php endif; ?>
	<form action="<?php echo JRoute::_('index.php?option=com_users&task=user.login'); ?>" method="post" class="jl-width-large jl-background-muted jl-padding">
		<fieldset>
			<?php foreach ($this->form->getFieldset('credentials') as $field) : ?>
				<?php if (!$field->hidden) : ?>
					<div class="control-label">
						<?php echo $field->label; ?>
					</div>
					<div class="jl-inline jl-width-1-1">
						<div class="controls">
							<span class="jl-form-icon jl-form-icon-flip" jl-icon="icon: arrow-right"></span>
							<?php echo $field->input; ?>
						</div>
					</div>
				<?php endif; ?>
			<?php endforeach; ?>
			<?php if ($this->tfa) : ?>
				<div class="control-label">
					<?php echo $this->form->getField('secretkey')->label; ?>
				</div>
				<div class="jl-inline jl-width-1-1">
					<div class="controls" jl-tooltip="<?php echo JText::_('JGLOBAL_SECRETKEY_HELP'); ?>">
						<span class="jl-form-icon jl-form-icon-flip" jl-icon="icon: lock"></span>
						<?php echo $this->form->getField('secretkey')->input; ?>
					</div>
				</div>
			<?php endif; ?>
			<?php if (JPluginHelper::isEnabled('system', 'remember')) : ?>
				<div class="jl-margin">
					<label for="remember"><input id="remember" class="jl-checkbox" type="checkbox" name="remember" class="inputbox" value="yes"> <?php echo JText::_('COM_USERS_LOGIN_REMEMBER_ME'); ?></label>
				</div>
			<?php endif; ?>
			<div class="control-group">
				<div class="controls">
					<button type="submit" class="jl-button jl-button-primary jl-width-1-1">
						<?php echo JText::_('JLOGIN'); ?>
					</button>
				</div>
			</div>
			<?php $return = $this->form->getValue('return', '', $this->params->get('login_redirect_url', $this->params->get('login_redirect_menuitem'))); ?>
			<input type="hidden" name="return" value="<?php echo base64_encode($return); ?>" />
			<?php echo JHtml::_('form.token'); ?>
		</fieldset>
	</form>

</div>
<div class="tm-form-login-extra jl-flex jl-flex-center">
<div class="jl-width-medium jl-text-center" jl-margin>

		<a class="jl-link-reset jl-text-small" href="<?php echo JRoute::_('index.php?option=com_users&view=reset'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_RESET'); ?>
		</a>


		<a class="jl-link-reset jl-text-small" href="<?php echo JRoute::_('index.php?option=com_users&view=remind'); ?>">
			<?php echo JText::_('COM_USERS_LOGIN_REMIND'); ?>
		</a>

	<?php $usersConfig = JComponentHelper::getParams('com_users'); ?>
	<?php if ($usersConfig->get('allowUserRegistration')) : ?>

			<a class="jl-link-reset jl-text-small" href="<?php echo JRoute::_('index.php?option=com_users&view=registration'); ?>">
				<?php echo JText::_('COM_USERS_LOGIN_REGISTER'); ?>
			</a>

	<?php endif; ?>
</div>
<div>
<div>
</div>
