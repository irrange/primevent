name: JL Dotnav
description: Create a dot navigation to scroll to different page sections.
type: particle
icon: fa-list
configuration:
  caching:
    type: static
form:
  overrideable: false
  fields:
    enabled:
      type: input.checkbox
      label: Enabled
      description: Globally enable to the particles.
      default: true
    _tabs:
      type: container.tabs
      fields:
        _tab_content:
          label: Content
          fields:
            items:
              overrideable: false
              type: collection.list
              array: true
              label: Items
              description: Create each item to appear in the content row.
              value: name
              ajax: true
              fields:
                .label:
                  type: input.text
                  label: Label
                  description: Add section label to scroll to (for tooltip label).
                  placeholder: Enter Tooltip Label.
                .link:
                  type: input.text
                  label: Link
                  description: Add section name to scroll, i.e: service
                  placeholder: Services
                .link_target:
                  type: select.selectize
                  label: Target
                  description: Target browser window when item is clicked.
                  placeholder: 'Select...'
                  default: _self
                  options:
                      _self: Self
                      _blank: New Window
            dotnav_settings:
              type: separator.note
              class: alert alert-info
              content: 'Dotnav Settings'
            offset:
              type: input.number
              label: Offset
              description: Pixel offset added to scroll top.
              placeholder: 0
              min: 0
            duration:
              type: input.number
              label: Duration
              description: Animation duration in milliseconds.
              placeholder: 1000
              min: 0
            dotnav_position:
              type: select.select
              label: Position
              description: Select the dotnav position.
              default: center-right
              options:
                top-left: Top Left
                top-center: Top Center
                top-right: Top Right
                center-left: Center Left
                center-center: Center Center
                center-right: Center Right
                bottom-left: Bottom Left
                bottom-center: Bottom Center
                bottom-right: Bottom Right
            vertical:
              type: input.checkbox
              label: Display Vertical
              default: true
            inverse_color:
              type: select.select
              label: Inverse Color
              description: Select light or dark mode to apply a light or dark color for better visibility.
              default: default
              options:
                default: Default
                light: Light
                dark: Dark
            tooltip:
              type: input.checkbox
              label: Show Tooltip
              default: false
            tooltip_position:
              type: select.select
              label: Tooltip Position
              description: Choose one of these options to adjust the tooltip's alignment.
              default: left
              options:
                top: Top
                bottom: Bottom
                left: Left
                right: Right
                top-left: Top Left
                top-right: Top Right
                bottom-left: Bottom Left
                bottom-right: Bottom Right
            class:
              type: input.selectize
              label: CSS Classes
              description: CSS class name for the particle.
        _tab_style:
          label: Style
          overridable: false
          fields:
            title_info:
              type: separator.note
              class: alert alert-info
              content: 'Set Inverse Color to default before using these style options.'
            dotnav_width:
              type: input.number
              label: Dotnav Width
              description: Enter the dotnav's width.
              min: 0
              placeholder: 10
            dotnav_height:
              type: input.number
              label: Dotnav Height
              description: Enter the dotnav's height.
              min: 0
              placeholder: 10
            border_color:
              type: input.colorpicker
              label: Border Color
              description: Customize the border color.
            background_color:
              type: input.colorpicker
              label: Background Color
              description: Customize the background color.
            dot_background_color:
              type: input.colorpicker
              label: Dotnav Background
              description: Customize the dotnav background color.
        _tab_general:
          label: General
          overridable: false
          fields:
            general_content_info:
              type: separator.note
              class: alert alert-info
              content: 'General Particle Settings'
            margin:
              type: select.select
              label: Margin
              description: Set the vertical margin.
              default: inherit
              options:
                  inherit: Keep existing
                  small: Small
                  default: Default
                  medium: Medium
                  large: Large
                  xlarge: X-Large
                  remove-vertical: None
            animation:
              type: select.select
              label: Animation
              description: Apply an animation to particles once they enter the viewport. This will animate all particles inside the section.
              default: inherit
              options:
                  inherit: None
                  fade: Fade
                  scale-up: Scale Up
                  scale-down: Scale Down
                  slide-top-small: Slide Top Small
                  slide-bottom-small: Slide Bottom Small
                  slide-left-small: Slide Left Small
                  slide-right-small: Slide Right Small
                  slide-top-medium: Slide Top Medium
                  slide-bottom-medium: Slide Bottom Medium
                  slide-left-medium: Slide Left Medium
                  slide-right-medium: Slide Right Medium
                  slide-top: Slide Top 100%
                  slide-bottom: Slide Bottom 100%
                  slide-left: Slide Left 100%
                  slide-right: Slide Right 100%
            animation_delay:
              type: input.number
              label: Animation Delay
              description: Set the delay animations for particle. Delay time in ms.
              min: 0
              placeholder: 200
            animation_repeat:
              type: select.select
              label: Animation Repeat
              description: Repeat an animation to particle once it enter the viewport.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            visibility:
              type: select.select
              label: Visibility
              description: Display the element only on this device width and larger.
              default: l
              options:
                  inherit: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
    copyright:
      type: separator.note
      class: alert alert-info
      content: 'JL Dotnav <strong>Version: 1.0.4</strong> Copyright (C) <a href="https://joomlead.com/" target="_blank">JoomLead</a>.'
