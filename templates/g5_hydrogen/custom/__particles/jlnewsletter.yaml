name: JL Newsletter
description: Display newsletter form.
type: particle
icon: fa-file-text-o
form:
  overrideable: false
  fields:
    enabled:
      type: input.checkbox
      label: Enabled
      description: Globally enable to the particles.
      default: true
    _tabs:
      type: container.tabs
      fields:
        _tab_content:
          label: Content
          fields:
            layout:
              type: select.select
              label: Layout
              description: Define the layout of the form.
              default: grid
              options:
                grid: Grid
                stacked: Stacked
                stackedname: Stacked (Name fields as grid)
            grid_gutter:
              type: select.select
              label: Gutter
              description: Set the gutter for the form fields.
              default: default
              options:
                small: Small
                default: Default
                medium: Medium
            form_size:
              type: select.select
              label: Size
              description: Modify the form fields size.
              default: default
              options:
                small: Small
                default: Default
                large: Large
            button_mode:
              type: select.select
              label: Button Mode
              description: Select whether a button or a clickable icon inside the email input is shown.
              default: icon
              options:
                icon: Icon
                button: Button
            uri:
              type: input.text
              label: Mailchimp URI
              description: Please put your Mailchimp Email Subscriptions URI here.
              placeholder: Mailchimp URI
            email_label:
              type: input.text
              label: Email Label
              description: Customize the label for email.
              placeholder: info@example.com
            button_label:
              type: input.text
              label: Button Label
              description: Customize the button label.
              placeholder: Subscribe
            buttonclass:
              type: select.select
              label: Button Style
              description: Set the button style.
              default: default
              options:
                  link: Link
                  link-muted: Link Muted
                  link-text: Link Text
                  default: Button Default
                  primary: Button Primary
                  secondary: Button Secondary
                  danger: Button Danger
                  text: Button Text
            link_button_size:
              type: select.select
              label: Button Size
              description: Set the button size. Button size not working with Link button style like Link, Link Muted, Link Text and Button text.
              default: default
              options:
                 small: Small
                 default: Default
                 large: Large
            fname_label:
              type: input.text
              label: First Name Label
              description: Customize the first name label.
              placeholder: First name
            lname_label:
              type: input.text
              label: Last Name Label
              description: Customize the last name label.
              placeholder: Last name
            show_namefields:
              type: input.checkbox
              label: Show Name Fields
              description: Show first name and last name fields.
              overridable: false
              default: true
            show_email_icon:
              type: input.checkbox
              label: Show Email Icon
              overridable: false
              default: true
            class:
              type: input.selectize
              label: CSS Classes
              description: CSS class name for the particle.
        _tab_style:
          label: Style
          overridable: false
          fields:
            success_settings:
              type: separator.note
              class: alert alert-info
              content: 'Success Style Settings'
            style:
              type: select.select
              label: Style
              description: Adding a status to the message to indicate a primary, success, warning or a danger status.
              default: success
              options:
                 default: Default
                 primary: Primary
                 success: Success
                 warning: Warning
                 danger: Danger
            style_extended:
              type: select.select
              label: Style Extended
              description: Adding a status to the message to indicate a primary, success, warning or a danger status.
              default: default
              options:
                 default: Default
                 border: Border
                 hivis: Hivis
            position:
              type: select.select
              label: Position
              description: Select a different position for this notification.
              default: top-center
              options:
                 top-left: Top Left
                 top-center: Top Center
                 top-right: Top Right
                 bottom-left: Bottom Left
                 bottom-center: Bottom Center
                 bottom-right: Bottom Right
            error_settings:
              type: separator.note
              class: alert alert-info
              content: 'Error Style Settings'
            error_style:
              type: select.select
              label: Style
              description: Adding a status to the message to indicate a primary, success, warning or a danger status.
              default: danger
              options:
                 default: Default
                 primary: Primary
                 success: Success
                 warning: Warning
                 danger: Danger
            error_style_extended:
              type: select.select
              label: Style Extended
              description: Adding a status to the message to indicate a primary, success, warning or a danger status.
              default: default
              options:
                 default: Default
                 border: Border
                 hivis: Hivis
            error_position:
              type: select.select
              label: Position
              description: Select a different position for this notification.
              default: top-center
              options:
                 top-left: Top Left
                 top-center: Top Center
                 top-right: Top Right
                 bottom-left: Bottom Left
                 bottom-center: Bottom Center
                 bottom-right: Bottom Right
            timeout:
              type: input.text
              label: Timeout
              description: Visibility duration until a notification disappears. Use 0 to remove the timeout duration.
              overridable: false
              default: 5000
              placeholder: 0
        _tab_general:
          label: General
          overridable: false
          fields:
            particle_title_info:
              type: separator.note
              class: alert alert-info
              content: 'Particle Title Style'
            particle_title:
              type: input.text
              label: Title
              description: Add an optional particle title.
              placeholder: Enter particle title
            particle_title_style:
              type: select.select
              label: Style
              description: Heading styles differ in font-size but may also come with a predefined color, size and font.
              default: h3
              options:
                 default: Default
                 heading-small: Small
                 heading-medium: Medium
                 heading-large: Large
                 heading-xlarge: XLarge
                 heading-2xlarge: 2XLarge
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
            particle_title_decoration:
              type: select.select
              label: Decoration
              description: Decorate the headline with a divider, bullet or a line that is vertically centered to the heading.
              default: none
              options:
                 none: None
                 divider: Divider
                 bullet: Bullet
                 line: Line
            particle_title_align:
              type: select.select
              label: Alignment
              description: Center, left and right alignment for Particle title.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            particle_predefined_color:
              type: select.select
              label: Predefined Color
              description: Select the text color. If the Background option is selected, styles that don't apply a background image use the primary color instead.
              default: default
              options:
                 default: Default
                 muted: Muted
                 emphasis: Emphasis
                 primary: Primary
                 secondary: Secondary
                 success: Success
                 warning: Warning
                 danger: Danger
            particle_title_color:
              type: input.colorpicker
              label: Custom Color
              description: Customize the title color instead using predefined color mode. Note: Set the Predefined color to default before using this color customization mode.
            particle_title_fontsize:
              type: input.number
              label: Font Size
              description: Customize the particle title font size.
              min: 0
            particle_title_element:
              type: select.select
              label: HTML Element
              description: Choose one of the elements to fit your semantic structure.
              default: h3
              options:
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
                 div: div
            general_content_info:
              type: separator.note
              class: alert alert-info
              content: 'General Particle Settings'
            align:
              type: select.select
              label: Text Alignment
              description: Center, left and right alignment may depend on a breakpoint and require a fallback.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            breakpoint:
              type: select.select
              label: Alignment Breakpoint
              description: Define the device width from which the alignment will apply.
              default: always
              options:
                  always: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            fallback:
              type: select.select
              label: Alignment Fallback
              description: Define an alignment fallback for device widths below the breakpoint.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            g_maxwidth:
              type: select.select
              label: Max Width
              description: Set the maximum content width.
              default: inherit
              options:
                  inherit: None
                  small: Small
                  medium: Medium
                  large: Large
                  xlarge: X-Large
                  xxlarge: XX-Large
            g_maxwidth_alignment:
              type: select.select
              label: Max Width Alignment
              description: Define the alignment in case the container exceeds the element's max-width.
              default: left
              options:
                  left: Left
                  center: Center
                  right: Right
            g_maxwidth_breakpoint:
              type: select.select
              label: Max Width Breakpoint
              description: Define the device width from which the element's max-width will apply.
              default: always
              options:
                  always: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            margin:
              type: select.select
              label: Margin
              description: Set the vertical margin.
              default: inherit
              options:
                  inherit: Keep existing
                  small: Small
                  default: Default
                  medium: Medium
                  large: Large
                  xlarge: X-Large
                  remove-vertical: None
            visibility:
              type: select.select
              label: Visibility
              description: Display the element only on this device width and larger.
              default: inherit
              options:
                  inherit: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            general_animation_info:
              type: separator.note
              class: alert alert-info
              content: 'Animation Settings'
            animation:
              type: select.select
              label: Animation
              description: Apply an animation to particles once they enter the viewport. This will animate all particles inside the section.
              default: inherit
              options:
                  inherit: None
                  fade: Fade
                  scale-up: Scale Up
                  scale-down: Scale Down
                  slide-top-small: Slide Top Small
                  slide-bottom-small: Slide Bottom Small
                  slide-left-small: Slide Left Small
                  slide-right-small: Slide Right Small
                  slide-top-medium: Slide Top Medium
                  slide-bottom-medium: Slide Bottom Medium
                  slide-left-medium: Slide Left Medium
                  slide-right-medium: Slide Right Medium
                  slide-top: Slide Top 100%
                  slide-bottom: Slide Bottom 100%
                  slide-left: Slide Left 100%
                  slide-right: Slide Right 100%
            animation_delay:
              type: input.number
              label: Animation Delay
              description: Set the delay animations for particle. Delay time in ms.
              min: 0
              placeholder: 200
            animation_repeat:
              type: select.select
              label: Animation Repeat
              description: Repeat an animation to particle once it enter the viewport.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
    copyright:
      type: separator.note
      class: alert alert-info
      content: 'JL Newsletter <strong>Version: 1.0.6</strong> Copyright (C) <a href="https://joomlead.com/" target="_blank">JoomLead</a>.'
