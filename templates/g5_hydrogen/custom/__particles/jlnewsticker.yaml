name: JL News Ticker
description: Create a news ticker.
type: particle
icon: fa-newspaper-o
form:
  overrideable: false
  fields:
    enabled:
      type: input.checkbox
      label: Enabled
      description: Globally enable particle.
      default: true
    _tabs:
      type: container.tabs
      fields:
        _tab_joomla:
          label: Joomla Content
          fields:
            title_text:
              type: input.text
              label: Title Text
              description: Customize the title text.
              default: Latest News
            joomla.categories:
              type: joomla.categories
              label: Joomla Categories
              description: Select the categories the articles should be taken from.
            joomla.articles:
              type: input.number
              label: Articles to Fetch
              description: Number of Joomla articles to fetch from category.
              min: 1
              default: 5
            joomla.introtext:
              type: select.select
              label: Intro Text
              description: Enable or disable the Intro Text.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            joomla.text.limit:
              type: input.text
              label: Text Limit
              description: Type in the number of characters the article text should be limited to.
              default: ''
              pattern: '\d+'
              placeholder: 50
            article.display.title.enabled:
              type: select.select
              label: Title
              description: Select if the article title should be shown.
              default: show
              options:
                show: Show
                '': Hide
            article.display.title.limit:
              type: input.text
              label: Title Limit
              description: Enter the maximum number of characters the article title should be limited to.
              pattern: '\d+(\.\d+){0,1}'
            joomla.date.enabled:
              type: select.select
              label: Date
              description: Select if the article date should be shown.
              default: published
              options:
                created: Show Created Date
                published: Show Published Date
                modified: Show Modified Date
                '': Hide
            joomla.date.format:
              type: select.date
              label: Date Format
              description: Select preferred date format. Leave empty not to display a date.
              default: l, d F
              selectize:
                  allowEmptyOption: true
              options:
                  'l, F d, Y': Date1
                  'l, d F': Date2
                  'D, d F': Date3
                  'F d': Date4
                  'd F': Date5
                  'd M': Date6
                  'D, M d, Y': Date7
                  'D, M d, y': Date8
                  'l': Date9
                  'l j F Y': Date10
                  'j F Y': Date11
                  'F d, Y': Date12
            joomla.link:
              type: select.select
              label: Link
              description: Enable or disable displaying of link to article.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            joomla.linktarget:
              type: select.selectize
              label: Target
              description: Target browser window when item is clicked.
              placeholder: 'Select...'
              default: _self
              options:
                  _self: Self
                  _blank: New Window
            class:
              type: input.selectize
              label: CSS Classes
              description: CSS class name for the particle.
            extra:
              type: collection.keyvalue
              label: Tag Attributes
              description: Extra Tag attributes.
              key_placeholder: Key (data-*, style, ...)
              value_placeholder: Value
              exclude: ['id', 'class']
        _tab_style:
          label: Style
          fields:
            card_settings:
              type: separator.note
              class: alert alert-info
              content: 'Title Layout'
            title_grid_width:
              type: select.select
              label: Grid Width
              description: Define the width of the ticker within the grid.
              default: auto
              options:
                auto: Auto
                1-2: 50%
                1-3: 33%
                1-4: 25%
                1-5: 20%
                small: Small
                medium: Medium
                large: Large
                xlarge: X-Large
                xxlarge: XX-Large
            title_grid_gutter:
              type: select.select
              label: Grid Gutter
              description: Select the gutter width between the ticker content and title.
              default: collapse
              options:
                small: Small
                medium: Medium
                default: Default
                large: Large
                collapse: Collapse
            title_grid_breakpoint:
              type: select.select
              label: Grid Breakpoint
              description: Set the breakpoint from which grid cells will stack.
              default: m
              options:
                default: Always
                s: Small (Phone Landscape)
                m: Medium (Tablet Landscape)
                l: Large (Desktop)
            title_vertical_alignment:
              type: input.checkbox
              label: Vertical Alignment
              description: Vertically center grid cells.
              default: false
            title_alignment:
              type: select.select
              label: Alignment
              description: Align the title to the left, right.
              default: left
              options:
                left: Left
                right: Right
            title_backgroundcolor:
              type: input.colorpicker
              label: Background
              description: Customize the title background color.
            title_color:
              type: input.colorpicker
              label: Color
              description: Customize the title color.
            title_fontsize:
              type: input.number
              label: Font Size
              min: 0
              description: Customize the title text font size
            title_text_transform:
              type: select.select
              label: Transform
              description: The following options will transform text into uppercased, capitalized or lowercased characters.
              default: ''
              options:
                 '': Inherit
                 uppercase: Uppercase
                 capitalize: Capitalize
                 lowercase: Lowercase
            content_settings:
              type: separator.note
              class: alert alert-info
              content: 'Common Style Settings'
            box_shadow:
              type: select.select
              label: Box Shadow
              description: Select the ticker's box shadow size. This option won't have any effect unless Card Style 'None' is selected.
              default: none
              options:
                none: None
                small: Small
                medium: Medium
                large: Large
                xlarge: X-Large
            box_shadow_hover:
              type: select.select
              label: Hover Box Shadow
              description: Select the ticker's box shadow size on hover.
              default: none
              options:
                none: None
                small: Small
                medium: Medium
                large: Large
                xlarge: X-Large
            extra_shadow:
              type: input.checkbox
              label: Extra Bottom Shadow
              description: This option won't have any effect unless Card Style 'None' is selected.
              default: false
            padding:
              type: input.number
              label: Padding
              description: Add spacing between elements and their content.
              min: 0
            news_ticker_settings:
              type: separator.note
              class: alert alert-info
              content: 'News Ticker Settings'
            card_style:
              type: select.select
              label: Background
              description: Select one of the boxed card styles or a blank panel.
              default: primary
              options:
                default: Default
                primary: Primary
                secondary: Secondary
                blank: None
                hover: Hover
            cardsize:
              type: select.select
              label: Content Size
              description: Define the card's size by selecting the padding between the card and its content.
              default: small
              options:
                default: Default
                small: Small
                large: Large
            content_transition:
              type: select.select
              label: Content Transition
              default: fade
              options:
                fade: Fade
                scale-up: Scale Up
                scale-down: Scale Down
                slide-top: Slide Top
                slide-bottom: Slide Bottom
                slide-left: Slide Left
                slide-right: Slide Right
                slide-top-small: Slide Top Small
                slide-bottom-small: Slide Bottom Small
                slide-left-small: Slide Left Small
                slide-right-small: Slide Right Small
                slide-top-medium: Slide Top Medium
                slide-bottom-medium: Slide Bottom Medium
                slide-left-medium: Slide Left Medium
                slide-right-medium: Slide Right Medium
            content_backgroundcolor:
              type: input.colorpicker
              label: Customize Background
              description: Customize the title background color.
            subtitle_color:
              type: input.colorpicker
              label: Content Color
              description: Customize the date color.
            subtitle_fontsize:
              type: input.number
              label: Content Font Size
              min: 0
              description: Customize the date font size
            content_text_transform:
              type: select.select
              label: Transform
              description: The following options will transform text into uppercased, capitalized or lowercased characters.
              default: ''
              options:
                 '': Inherit
                 uppercase: Uppercase
                 capitalize: Capitalize
                 lowercase: Lowercase
        _tab_settings:
          label: Slider
          fields:
            slider_gutter:
              type: select.select
              label: Gutter
              description: Set the grid gutter width and display dividers between grid cells.
              default: default
              options:
                small: Small
                medium: Medium
                default: Default
                large: Large
                collapse: Collapse
            slider_divider:
              type: select.select
              label: Show dividers
              description: Display dividers between grid cells
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            slider_animation_info:
              type: separator.note
              class: alert alert-info
              content: 'Animation'
            slider_animation_set:
              type: select.select
              label: Set
              description: Slide all visible items at once. Group items into sets. The number of items within a set depends on the defined item width, e.g. 33% means that each set contains 3 items.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            slider_animation_center:
              type: select.select
              label: Center
              description: Center the active slide
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            slider_animation_finite:
              type: select.select
              label: Finite
              description: Disable infinite scrolling
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            slider_animation_autoplay:
              type: select.select
              label: Autoplay
              description: Enable autoplay for carousel items.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            slider_animation_interval:
              type: input.number
              label: Interval
              description: Set the autoplay interval in seconds. Min 5 Max 15.
              min: 5
              max: 15
              placeholder: 7
            column_info:
              type: separator.note
              class: alert alert-info
              content: 'Item Width - You need to set the Item Width Mode to FIXED mode.'
            phone_portrait:
              type: select.select
              label: Phone Portrait
              description: Set the item width for each breakpoint. Inherit refers to the item width of the next smaller screen size.
              default: 1-1
              options:
                1-1: 100%
                5-6: 83%
                4-5: 80%
                3-5: 60%
                1-2: 50%
                1-3: 33%
                1-4: 25%
                1-5: 20%
                1-6: 16%
            phone_landscape:
              type: select.select
              label: Phone Landscape
              description: Set the item width for each breakpoint. Inherit refers to the item width of the next smaller screen size.
              default: inherit
              options:
                inherit: Inherit
                1-1: 100%
                5-6: 83%
                4-5: 80%
                3-5: 60%
                1-2: 50%
                1-3: 33%
                1-4: 25%
                1-5: 20%
                1-6: 16%
            tablet_landscape:
              type: select.select
              label: Tablet Landscape
              description: Set the item width for each breakpoint. Inherit refers to the item width of the next smaller screen size.
              default: 1-1
              options:
                inherit: Inherit
                1-1: 100%
                5-6: 83%
                4-5: 80%
                3-5: 60%
                1-2: 50%
                1-3: 33%
                1-4: 25%
                1-5: 20%
                1-6: 16%
            desktop:
              type: select.select
              label: Desktop
              description: Set the item width for each breakpoint. Inherit refers to the item width of the next smaller screen size.
              default: inherit
              options:
                inherit: Inherit
                1-1: 100%
                5-6: 83%
                4-5: 80%
                3-5: 60%
                1-2: 50%
                1-3: 33%
                1-4: 25%
                1-5: 20%
                1-6: 16%
            large_desktop:
              type: select.select
              label: Large Screens
              description: Set the item width for each breakpoint. Inherit refers to the item width of the next smaller screen size.
              default: inherit
              options:
                inherit: Inherit
                1-1: 100%
                5-6: 83%
                4-5: 80%
                3-5: 60%
                1-2: 50%
                1-3: 33%
                1-4: 25%
                1-5: 20%
                1-6: 16%
            slidenav_info:
              type: separator.note
              class: alert alert-info
              content: 'Slidenav'
            slidenav_position:
              type: select.select
              label: Position
              description: Align the navigation's items.
              default: center-right
              options:
                none: None
                default: Default
                center-left: Center Left
                center-right: Center Right
            slidenav_hover:
              type: select.select
              label: Show On Hover
              description: Show the slide nav on hover only.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
            slidenav_margin:
              type: select.select
              label: Margin
              description: Set the vertical margin.
              default: none
              options:
                none: None
                small: Small
                medium: Medium
                large: Large
            slidenav_breakpoint:
              type: select.select
              label: Breakpoint
              description: Display the slidenav only on this device width and larger.
              default: s
              options:
                always: Always
                s: Small (Phone Landscape)
                m: Medium (Tablet Landscape)
                l: Large (Desktop)
                xl: X-Large (Large Screens)
            slidenav_color:
              type: select.select
              label: Color
              description: Set light or dark color mode.
              default: default
              options:
                default: Default
                light: Light
                dark: Dark
        _tab_general:
          label: General
          fields:
            particle_title_info:
              type: separator.note
              class: alert alert-info
              content: 'Particle Title Style'
            particle_title:
              type: input.text
              label: Title
              description: Add an optional particle title.
              placeholder: Enter particle title
            particle_title_style:
              type: select.select
              label: Style
              description: Heading styles differ in font-size but may also come with a predefined color, size and font.
              default: h3
              options:
                 default: Default
                 heading-small: Small
                 heading-medium: Medium
                 heading-large: Large
                 heading-xlarge: XLarge
                 heading-2xlarge: 2XLarge
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
            particle_title_decoration:
              type: select.select
              label: Decoration
              description: Decorate the headline with a divider, bullet or a line that is vertically centered to the heading.
              default: none
              options:
                 none: None
                 divider: Divider
                 bullet: Bullet
                 line: Line
            particle_title_align:
              type: select.select
              label: Alignment
              description: Center, left and right alignment for Particle title.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            particle_predefined_color:
              type: select.select
              label: Predefined Color
              description: Select the text color. If the Background option is selected, styles that don't apply a background image use the primary color instead.
              default: default
              options:
                 default: Default
                 muted: Muted
                 emphasis: Emphasis
                 primary: Primary
                 secondary: Secondary
                 success: Success
                 warning: Warning
                 danger: Danger
            particle_title_color:
              type: input.colorpicker
              label: Custom Color
              description: Customize the title color instead using predefined color mode. Note: Set the Predefined color to default before using this color customization mode.
            particle_title_fontsize:
              type: input.number
              label: Font Size
              description: Customize the particle title font size.
              min: 0
            particle_title_element:
              type: select.select
              label: HTML Element
              description: Choose one of the elements to fit your semantic structure.
              default: h3
              options:
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
                 div: div
            general_content_info:
              type: separator.note
              class: alert alert-info
              content: 'General Particle Settings'
            align:
              type: select.select
              label: Text Alignment
              description: Center, left and right alignment may depend on a breakpoint and require a fallback.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            breakpoint:
              type: select.select
              label: Alignment Breakpoint
              description: Define the device width from which the alignment will apply.
              default: always
              options:
                  always: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            fallback:
              type: select.select
              label: Alignment Fallback
              description: Define an alignment fallback for device widths below the breakpoint.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            g_maxwidth:
              type: select.select
              label: Max Width
              description: Set the maximum content width.
              default: inherit
              options:
                  inherit: None
                  small: Small
                  medium: Medium
                  large: Large
                  xlarge: X-Large
                  xxlarge: XX-Large
            g_maxwidth_alignment:
              type: select.select
              label: Max Width Alignment
              description: Define the alignment in case the container exceeds the element's max-width.
              default: left
              options:
                  left: Left
                  center: Center
                  right: Right
            g_maxwidth_breakpoint:
              type: select.select
              label: Max Width Breakpoint
              description: Define the device width from which the element's max-width will apply.
              default: always
              options:
                  always: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            margin:
              type: select.select
              label: Margin
              description: Set the vertical margin.
              default: inherit
              options:
                  inherit: Keep existing
                  small: Small
                  default: Default
                  medium: Medium
                  large: Large
                  xlarge: X-Large
                  remove-vertical: None
            visibility:
              type: select.select
              label: Visibility
              description: Display the element only on this device width and larger.
              default: inherit
              options:
                  inherit: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            general_animation_info:
              type: separator.note
              class: alert alert-info
              content: 'Animation Settings'
            animation:
              type: select.select
              label: Animation
              description: Apply an animation to particles once they enter the viewport. This will animate all particles inside the section.
              default: inherit
              options:
                  inherit: None
                  fade: Fade
                  scale-up: Scale Up
                  scale-down: Scale Down
                  slide-top-small: Slide Top Small
                  slide-bottom-small: Slide Bottom Small
                  slide-left-small: Slide Left Small
                  slide-right-small: Slide Right Small
                  slide-top-medium: Slide Top Medium
                  slide-bottom-medium: Slide Bottom Medium
                  slide-left-medium: Slide Left Medium
                  slide-right-medium: Slide Right Medium
                  slide-top: Slide Top 100%
                  slide-bottom: Slide Bottom 100%
                  slide-left: Slide Left 100%
                  slide-right: Slide Right 100%
            animation_delay:
              type: input.number
              label: Animation Delay
              description: Set the delay animations for particle. Delay time in ms.
              min: 0
              placeholder: 200
            animation_repeat:
              type: select.select
              label: Animation Repeat
              description: Repeat an animation to particle once it enter the viewport.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
    copyright:
      type: separator.note
      class: alert alert-info
      content: 'JL News Ticker <strong>Version: 2.0.8</strong> Copyright (C) <a href="https://joomlead.com/" target="_blank">JoomLead</a>.'
