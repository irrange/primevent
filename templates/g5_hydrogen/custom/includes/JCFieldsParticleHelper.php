<?php

class JCFieldsParticleHelper

{

    public function getFields($id)
    {
        
        if (!class_exists('ContentModelArticle')) {
            require_once JPATH_ROOT . '/components/com_content/models/article.php';
        };
        $model = new ContentModelArticle;

        $appParams = JFactory::getApplication()->getParams();
        $model->setState('params', $appParams);

        $item = $model->getItem($id);
        $jcFields = FieldsHelper::getFields('com_content.article', $item, true);

        foreach ($jcFields as $jcField) {
            $jcFields[$jcField->name] = $jcField;
        }

        return $jcFields ? $jcFields : null;
    }
}

