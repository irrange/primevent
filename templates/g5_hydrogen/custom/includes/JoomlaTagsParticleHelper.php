<?php

class JoomlaTagsParticleHelper
{
    static $tagsData = [];

    /**
     * @param array $ids
     *
     * @return array
     */
    public function getTagsByArticleIds(array $ids = [])
    {

        $result = [];

        for ($i = 0; $i < count($ids); $i++) {
            if (!isset(self::$tagsData[$ids[$i]])) {
                self::$tagsData[$ids[$i]] = (new JHelperTags)->getItemTags('com_content.article', $ids[$i]);
            }
            $tags = self::$tagsData[$ids[$i]];

            for ($z = 0; $z < count($tags); $z++) {
                $tag = $tags[$z];
                $result[$tag->tag_id] = $tag->title;
            }
        }

        return $result;
    }
}