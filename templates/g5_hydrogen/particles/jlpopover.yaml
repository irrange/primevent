name: JL Popover
description: Display an image with markers, which open popovers.
type: particle
icon: fa-list
configuration:
  caching:
    type: static
form:
  overrideable: false
  fields:
    enabled:
      type: input.checkbox
      label: Enabled
      description: Globally enable to the particles.
      default: true
    _tabs:
      type: container.tabs
      fields:
        _tab_content:
          label: Content
          fields:
            image:
              type: input.imagepicker
              label: Image
              description: An image field with an image picker.
              placeholder: Pick an image
            image_alt:
              type: input.text
              label: Image ALT
              description: Enter the image's alt attribute.
              placeholder: Image ALT
            items:
              overrideable: false
              type: collection.list
              array: true
              label: Items
              description: Create each item to appear in the content row.
              value: name
              ajax: true
              fields:
                position_settings:
                  type: separator.note
                  class: alert alert-info
                  content: 'Enter the horizontal and vertical position of the marker in percent.'
                .item_left:
                  type: input.number
                  label: Left
                  placeholder: 50
                  min: 0
                  max: 100
                .item_top:
                  type: input.number
                  label: Top
                  placeholder: 50
                  min: 0
                  max: 100
                .title:
                  type: input.text
                  label: Title
                  description: Customize the Title.
                  placeholder: Enter Title
                .meta:
                  type: input.text
                  label: Meta
                  description: Customize the Meta.
                  placeholder: Enter Meta
                .content:
                  type: textarea.textarea
                  label: Description
                  description: Customize the description.
                  placeholder: Enter Description
                .item_image:
                  type: input.imagepicker
                  label: Image
                  description: An image field with an image picker.
                  placeholder: Pick an image
                .item_image_alt:
                  type: input.text
                  label: Image ALT
                  description: Enter the image's alt attribute.
                  placeholder: Image ALT
                .link_text:
                  type: input.text
                  label: Link Text
                  description: Specify the button text.
                  placeholder: Read more
                .link:
                  type: input.text
                  label: Link
                  description: Specify the link for button. <br/>Note: Scroll smoothly added automatically to internal link that contains a URL fragment to add the smooth scrolling behavior.
                  placeholder: http://
                .link_target:
                  type: select.selectize
                  label: Target
                  description: Open the link in a same or new window.
                  placeholder: 'Select...'
                  default: _self
                  options:
                      _self: Self
                      _blank: New Window
                .item_position:
                  type: select.select
                  label: Position
                  description: Select a different position for this item.
                  default: default
                  options:
                     default: Default
                     top-center: Top
                     bottom-center: Bottom
                     left-center: Left
                     right-center: Right
            class:
              type: input.selectize
              label: CSS Classes
              description: CSS class name for the particle.
        _tab_style:
          label: Style
          overridable: false
          fields:
            popover_settings:
              type: separator.note
              class: alert alert-info
              content: 'Popover Settings'
            popover_mode:
              type: select.select
              label: Mode
              description: Display the popover on click or hover.
              default: hover
              options:
                 hover: Hover
                 click: Click
            popover_position:
              type: select.select
              label: Position
              description: Select the popover's alignment to its marker. If the popover doesn't fit its container, it will flip automatically.
              default: top-center
              options:
                 top-center: Top
                 bottom-center: Bottom
                 left-center: Left
                 right-center: Right
            popover_width:
              type: input.number
              label: Width
              description: Enter a width for the popover in pixel.
              min: 0
              placeholder: 300
            popover_styles:
              type: select.select
              label: Style
              description: Select a card style.
              default: default
              options:
                default: Default
                primary: Primary
                secondary: Secondary
            drop_animation:
              type: select.select
              label: Animation
              description: Select animation on hover/click.
              default: inherit
              options:
                  inherit: None
                  fade: Fade
                  scale-up: Scale Up
                  scale-down: Scale Down
                  slide-top-small: Slide Top Small
                  slide-bottom-small: Slide Bottom Small
                  slide-left-small: Slide Left Small
                  slide-right-small: Slide Right Small
                  slide-top-medium: Slide Top Medium
                  slide-bottom-medium: Slide Bottom Medium
                  slide-left-medium: Slide Left Medium
                  slide-right-medium: Slide Right Medium
                  slide-top: Slide Top 100%
                  slide-bottom: Slide Bottom 100%
                  slide-left: Slide Left 100%
                  slide-right: Slide Right 100%
            popover_size:
              type: select.select
              label: Size
              description: Define the card's size by selecting the padding between the card and its content.
              default: default
              options:
                small: Small
                default: Default
                large: Large
            popover_image_width:
              type: input.number
              label: Image Width
              description: Enter the popover image's width
              min: 0
              placeholder: auto
            popover_image_height:
              type: input.number
              label: Image Height
              description: Enter the popover image's height
              min: 0
              placeholder: auto
            title_settings:
              type: separator.note
              class: alert alert-info
              content: 'Title Settings'
            title_style:
              type: select.select
              label: Style
              description: Select the title style and add an optional colon at the end of the title.
              default: default
              options:
                 default: Default
                 heading-primary: Primary
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
            title_decoration:
              type: select.select
              label: Decoration
              description: Decorate the headline with a divider, bullet or a line that is vertically centered to the heading.
              default: none
              options:
                 none: None
                 divider: Divider
                 bullet: Bullet
                 line: Line
            title_color:
              type: select.select
              label: Predefined Color
              description: Select the title text color. If the Background option is selected, styles that don't apply a background image use the primary color instead.
              default: default
              options:
                 default: Default
                 muted: Muted
                 emphasis: Emphasis
                 primary: Primary
                 secondary: Secondary
                 success: Success
                 warning: Warning
                 danger: Danger
                 background: Background
            customize_title_color:
              type: input.colorpicker
              label: Custom Color
              description: Customize the title color instead using predefined title color mode. <br>Note: You need to set the predefined color to default before using the color customization.
            customize_title_fontsize:
              type: input.number
              label: Font Size
              min: 0
              description: Customize the title text font size.
            title_text_transform:
              type: select.select
              label: Transform
              description: The following options will transform text into uppercased, capitalized or lowercased characters.
              default: ''
              options:
                 '': Inherit
                 uppercase: Uppercase
                 capitalize: Capitalize
                 lowercase: Lowercase
            title_element:
              type: select.select
              label: HTML Element
              description: Choose one of the elements to fit your semantic structure.
              default: h3
              options:
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
                 div: div
            title_margin_top:
              type: select.select
              label: Margin Top
              description: Set the top margin.
              default: default
              options:
                 small: Small
                 default: Default
                 medium: Medium
                 large: Large
                 xlarge: X-Large
                 remove: None
            meta_settings:
              type: separator.note
              class: alert alert-info
              content: 'Meta Settings'
            meta_style:
              type: select.select
              label: Style
              description: Select a predefined meta text style, including color, size and font-family.
              default: text-meta
              options:
                 default: Default
                 text-meta: Meta
                 heading-2xlarge: 2XLarge
                 heading-xlarge: XLarge
                 heading-large: Large
                 heading-medium: Medium
                 heading-small: Small
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
            meta_predefined_color:
              type: select.select
              label: Predefined Color
              description: Select the content text color. If the Background option is selected, styles that don't apply a background image use the primary color instead.
              default: default
              options:
                 default: Default
                 muted: Muted
                 emphasis: Emphasis
                 primary: Primary
                 secondary: Secondary
                 success: Success
                 warning: Warning
                 danger: Danger
            meta_color:
              type: input.colorpicker
              label: Custom Color
              description: Customize the meta color. <br>Note: You need to set the predefined color to default before using the color customization.
            meta_fontsize:
              type: input.number
              label: Font Size
              min: 0
              description: Customize the meta text font size
            meta_text_transform:
              type: select.select
              label: Transform
              description: The following options will transform text into uppercased, capitalized or lowercased characters.
              default: ''
              options:
                 '': Inherit
                 uppercase: Uppercase
                 capitalize: Capitalize
                 lowercase: Lowercase
            meta_alignment:
              type: select.select
              label: Alignment
              description: Align the meta text above/below the title or below the content.
              default: bottom
              options:
                top: Above Title
                bottom: Below Title
                content: Below Content
            meta_margin_top:
              type: select.select
              label: Margin Top
              description: Set the top margin.
              default: default
              options:
                 small: Small
                 default: Default
                 medium: Medium
                 large: Large
                 xlarge: X-Large
                 remove: None
            content_settings:
              type: separator.note
              class: alert alert-info
              content: 'Content Settings'
            content_style:
              type: select.select
              label: Style
              description: Select a predefined meta text style, including color, size and font-family.
              default: default
              options:
                 default: Default
                 lead: Lead
            content_text_color:
              type: select.select
              label: Predefined Color
              description: Select the content text color. If the Background option is selected, styles that don't apply a background image use the primary color instead.
              default: default
              options:
                 default: Default
                 muted: Muted
                 emphasis: Emphasis
                 primary: Primary
                 secondary: Secondary
                 success: Success
                 warning: Warning
                 danger: Danger
            customize_content_color:
              type: input.colorpicker
              label: Custom Color
              description: Customize the content color instead using predefined text color. <br>Note: You need to set the predefined color to default to use the customize color option.
            customize_content_fontsize:
              type: input.number
              label: Font Size
              min: 0
              description: Customize the content text font size
            content_text_transform:
              type: select.select
              label: Transform
              description: The following options will transform text into uppercased, capitalized or lowercased characters.
              default: ''
              options:
                 '': Inherit
                 uppercase: Uppercase
                 capitalize: Capitalize
                 lowercase: Lowercase
            content_margin_top:
              type: select.select
              label: Margin Top
              description: Set the top margin.
              default: default
              options:
                 small: Small
                 default: Default
                 medium: Medium
                 large: Large
                 xlarge: X-Large
                 remove: None
            image_settings:
              type: separator.note
              class: alert alert-info
              content: 'Image Settings'
            image_width:
              type: input.number
              label: Width
              description: Enter the image's width
              min: 0
              placeholder: auto
            image_height:
              type: input.number
              label: Height
              description: Enter the image's height
              min: 0
              placeholder: auto
            image_padding:
              type: input.checkbox
              label: Remove Padding
              default: true
            image_border:
              type: select.select
              label: Border
              description: Select the image's border style. This option won't have any effect if Align image without padding is enabled
              default: none
              options:
                none: None
                circle: Circle
                rounded: Rounded
                pill: Pill
            box_shadow:
              type: select.select
              label: Box Shadow
              description: Select the image's box shadow size.
              default: none
              options:
                none: None
                small: Small
                medium: Medium
                large: Large
                xlarge: X-Large
            link_settings:
              type: separator.note
              class: alert alert-info
              content: 'Link Settings'
            link_type:
              type: select.select
              label: Type
              description: Show the link as a button or choose between linking just the image and title or the whole particle.<br/>Note: Style/Button Size and Margin top no longer working if you set link type to Item
              default: button
              options:
                  button: Button
                  content: Title/Image
                  element: Item
            link_style:
              type: select.select
              label: Style
              description: Set the button style.
              default: default
              options:
                  default: Default
                  primary: Primary
                  secondary: Secondary
                  danger: Danger
                  text: Text
                  link: Link
                  link-muted: Link Muted
                  link-text: Link Text
            link_button_size:
              type: select.select
              label: Size
              description: Set the button size.
              default: default
              options:
                 small: Small
                 default: Default
                 large: Large
            link_margin_top:
              type: select.select
              label: Margin Top
              description: Set the top margin.
              default: default
              options:
                 small: Small
                 default: Default
                 medium: Medium
                 large: Large
                 xlarge: X-Large
                 remove: None
        _tab_general:
          label: General
          overridable: false
          fields:
            particle_title_info:
              type: separator.note
              class: alert alert-info
              content: 'Particle Title Style'
            particle_title:
              type: input.text
              label: Title
              description: Add an optional particle title.
              placeholder: Enter particle title
            particle_title_style:
              type: select.select
              label: Style
              description: Heading styles differ in font-size but may also come with a predefined color, size and font.
              default: h3
              options:
                 default: Default
                 heading-small: Small
                 heading-medium: Medium
                 heading-large: Large
                 heading-xlarge: XLarge
                 heading-2xlarge: 2XLarge
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
            particle_title_decoration:
              type: select.select
              label: Decoration
              description: Decorate the headline with a divider, bullet or a line that is vertically centered to the heading.
              default: none
              options:
                 none: None
                 divider: Divider
                 bullet: Bullet
                 line: Line
            particle_title_align:
              type: select.select
              label: Alignment
              description: Center, left and right alignment for Particle title.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            particle_predefined_color:
              type: select.select
              label: Predefined Color
              description: Select the text color. If the Background option is selected, styles that don't apply a background image use the primary color instead.
              default: default
              options:
                 default: Default
                 muted: Muted
                 emphasis: Emphasis
                 primary: Primary
                 secondary: Secondary
                 success: Success
                 warning: Warning
                 danger: Danger
            particle_title_color:
              type: input.colorpicker
              label: Custom Color
              description: Customize the title color instead using predefined color mode. Note: Set the Predefined color to default before using this color customization mode.
            particle_title_fontsize:
              type: input.number
              label: Font Size
              description: Customize the particle title font size.
              min: 0
            particle_title_element:
              type: select.select
              label: HTML Element
              description: Choose one of the elements to fit your semantic structure.
              default: h3
              options:
                 h1: H1
                 h2: H2
                 h3: H3
                 h4: H4
                 h5: H5
                 h6: H6
                 div: div
            general_content_info:
              type: separator.note
              class: alert alert-info
              content: 'General Particle Settings'
            align:
              type: select.select
              label: Text Alignment
              description: Center, left and right alignment may depend on a breakpoint and require a fallback.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            breakpoint:
              type: select.select
              label: Alignment Breakpoint
              description: Define the device width from which the alignment will apply.
              default: always
              options:
                  always: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            fallback:
              type: select.select
              label: Alignment Fallback
              description: Define an alignment fallback for device widths below the breakpoint.
              default: inherit
              options:
                  inherit: Inherit
                  left: Left
                  center: Center
                  right: Right
                  justify: Justify
            g_maxwidth:
              type: select.select
              label: Max Width
              description: Set the maximum content width.
              default: inherit
              options:
                  inherit: None
                  small: Small
                  medium: Medium
                  large: Large
                  xlarge: X-Large
                  xxlarge: XX-Large
            g_maxwidth_alignment:
              type: select.select
              label: Max Width Alignment
              description: Define the alignment in case the container exceeds the element's max-width.
              default: left
              options:
                  left: Left
                  center: Center
                  right: Right
            g_maxwidth_breakpoint:
              type: select.select
              label: Max Width Breakpoint
              description: Define the device width from which the element's max-width will apply.
              default: always
              options:
                  always: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            margin:
              type: select.select
              label: Margin
              description: Set the vertical margin.
              default: inherit
              options:
                  inherit: Keep existing
                  small: Small
                  default: Default
                  medium: Medium
                  large: Large
                  xlarge: X-Large
                  remove-vertical: None
            visibility:
              type: select.select
              label: Visibility
              description: Display the element only on this device width and larger.
              default: inherit
              options:
                  inherit: Always
                  s: Small (Phone Landscape)
                  m: Medium (Tablet Landscape)
                  l: Large (Desktop)
                  xl: X-Large (Large Screens)
            general_animation_info:
              type: separator.note
              class: alert alert-info
              content: 'Animation Settings'
            animation:
              type: select.select
              label: Animation
              description: Apply an animation to particles once they enter the viewport. This will animate all particles inside the section.
              default: inherit
              options:
                  inherit: None
                  fade: Fade
                  scale-up: Scale Up
                  scale-down: Scale Down
                  slide-top-small: Slide Top Small
                  slide-bottom-small: Slide Bottom Small
                  slide-left-small: Slide Left Small
                  slide-right-small: Slide Right Small
                  slide-top-medium: Slide Top Medium
                  slide-bottom-medium: Slide Bottom Medium
                  slide-left-medium: Slide Left Medium
                  slide-right-medium: Slide Right Medium
                  slide-top: Slide Top 100%
                  slide-bottom: Slide Bottom 100%
                  slide-left: Slide Left 100%
                  slide-right: Slide Right 100%
                  parallax: Parallax
            animation_delay:
              type: input.number
              label: Animation Delay
              description: Set the delay animations for particle. Delay time in ms.
              min: 0
              placeholder: 200
            animation_repeat:
              type: select.select
              label: Animation Repeat
              description: Repeat an animation to particle once it enter the viewport.
              default: disabled
              options:
                enabled: Enable
                disabled: Disable
        _tab_parallax_animation:
          label: Parallax
          overridable: false
          fields:
            parallax_info:
              type: separator.note
              class: alert alert-info
              content: 'To use parallax animation, you need to select the parallax animation type in General tab (Animation field). Hovering the name of a setting will show a tooltip with a brief explanation on the left side.'
            pa_horizontal_start:
              type: input.number
              label: Horizontal Start
              description: Animate the horizontal position (translateX) in pixels. Min: -600 and Max: 600.
              min: -600
              max: 600
              placeholder: 0
            pa_horizontal_end:
              type: input.number
              label: Horizontal End
              description: Animate the horizontal position (translateX) in pixels. Min: -600 and Max: 600.
              min: -600
              max: 600
              placeholder: 0
            pa_vertical_start:
              type: input.number
              label: Vertical Start
              description: Animate the vertical position (translateY) in pixels. Min: -600 and Max: 600.
              min: -600
              max: 600
              placeholder: 0
            pa_vertical_end:
              type: input.number
              label: Vertical End
              description: Animate the vertical position (translateY) in pixels. Min: -600 and Max: 600.
              min: -600
              max: 600
              placeholder: 0
            scale_start:
              type: input.number
              label: Scale Start
              description: Animate the scaling. 100 means 100% scale, 200 means 200% scale, and 50 means 50% scale. NOTE: Min 50 and Max 200
              min: 50
              max: 200
              placeholder: 100
            scale_end:
              type: input.number
              label: Scale End
              description: Animate the scaling. 100 means 100% scale, 200 means 200% scale, and 50 means 50% scale. NOTE: Min 50 and Max 200
              min: 50
              max: 200
              placeholder: 100
            rotate_start:
              type: input.number
              label: Rotate Start
              description: Animate the rotation clockwise in degrees. NOTE: Min 0 and Max 360
              min: 0
              max: 360
              placeholder: 0
            rotate_end:
              type: input.number
              label: Rotate End
              description: Animate the rotation clockwise in degrees. NOTE: Min 0 and Max 360
              min: 0
              max: 360
              placeholder: 0
            opacity_start:
              type: input.number
              label: Opacity Start
              description: Animate the opacity. 100 means 100% opacity, 0 means 0% opacity and 50 means 50%. NOTE: Min 0 and Max 100
              min: 0
              max: 100
              placeholder: 100
            opacity_end:
              type: input.number
              label: Opacity End
              description: Animate the opacity. 100 means 100% opacity, 0 means 0% opacity and 50 means 50%. NOTE: Min 0 and Max 100
              min: 0
              max: 100
              placeholder: 100
            easing:
              type: input.number
              label: Easing
              description: Determine how the speed of the animation behaves over time. A value below 100 is faster in the beginning and slower towards the end while a value above 100 behaves inversely. Min 10 and Max 200
              min: 10
              max: 200
              placeholder: 10
            pa_viewport:
              type: input.number
              label: Viewport
              description: Set the animation end point relative to viewport height, e.g. 50 for 50% of the viewport. Min 10 and Max 100
              min: 10
              max: 100
              placeholder: 50
            pa_breakpoint:
              type: select.select
              label: Breakpoint
              description: Display the parallax effect only on this device width and larger.
              default: always
              options:
                always: Always
                s: Small (Phone Landscape)
                m: Medium (Tablet Landscape)
                l: Large (Desktop)
                xl: X-Large (Large Screens)
        _tab_parallax:
          label: Parallax Background
          overridable: false
          fields:
            parallax_bg_info:
              type: separator.note
              class: alert alert-info
              content: 'The Parallax Background settings allow you to animate a background image depending on the scroll position of the document.'
            parallax_image:
              type: input.imagepicker
              label: Background Image
              description: Select parallax background image for particle.
              placeholder: Pick an image
            background_image_size:
              type: select.select
              label: Image Size
              description: Determine whether the image will fit the section dimensions by clipping it or by filling the empty areas with the background color.
              default: auto
              options:
                auto: Auto
                cover: Cover
                contain: Contain
            background_image_position:
              type: select.select
              label: Image Position
              description: Set the initial background position, relative to the section layer.
              default: center-center
              options:
                top-left: Top Left
                top-center: Top Center
                top-right: Top Right
                center-left: Center Left
                center-center: Center Center
                center-right: Center Right
                bottom-left: Bottom Left
                bottom-center: Bottom Center
                bottom-right: Bottom Right
            parallax_bg_breakpoint:
              type: select.select
              label: Parallax Breakpoint
              description: Display the parallax effect only on this device width and larger.
              default: always
              options:
                always: Always
                s: Small (Phone Landscape)
                m: Medium (Tablet Landscape)
                l: Large (Desktop)
                xl: X-Large (Large Screens)
            parallax_bg_visibility:
              type: select.select
              label: Image Visibility
              description: Display the image only on this device width and larger.
              default: always
              options:
                always: Always
                s: Small (Phone Landscape)
                m: Medium (Tablet Landscape)
                l: Large (Desktop)
                xl: X-Large (Large Screens)
            parallax_bg_color:
              type: input.colorpicker
              label: Background Color
              description: Use the background color in combination with blend modes, a transparent image or to fill the area, if the image doesn't cover the whole section.
            blendmode:
              type: select.select
              label: Blend Mode
              description: Determine how the image will blend with the background color.
              default: inherit
              options:
                  inherit: Normal
                  multiply: Multiply
                  screen: Screen
                  overlay: Overlay
                  darken: Darken
                  lighten: Lighten
                  color-dodge: Color-dodge
                  color-burn: Color-burn
                  hard-light: Hard-light
                  soft-light: Soft-light
                  difference: Difference
                  exclusion: Exclusion
                  hue: Hue
                  saturation: Saturation
                  color: Color
                  luminosity: Luminosity
            parallax_bg_overlay:
              type: input.colorpicker
              label: Overlay Color
              description: Set an additional transparent overlay to soften the image.
            horizontal_start:
              type: input.number
              label: Horizontal Start
              description: Animate the horizontal position (translateX) in pixels. Min: -600 and Max: 600.
              min: -600
              max: 600
              default: 0
            horizontal_end:
              type: input.number
              label: Horizontal End
              description: Animate the horizontal position (translateX) in pixels. Min: -600 and Max: 600.
              min: -600
              max: 600
              default: 0
            vertical_start:
              type: input.number
              label: Vertical Start
              description: Animate the vertical position (translateY) in pixels. Min: -600 and Max: 600.
              min: -600
              max: 600
              default: 0
            vertical_end:
              type: input.number
              label: Vertical End
              description: Animate the vertical position (translateY) in pixels. Min: -600 and Max: 600.
              min: -600
              max: 600
              default: 0
            container:
              type: input.checkbox
              label: Add Container
              description: If you set the Fullwidth (Flushed Content) for parent section, you can use this option to define the container width for the particles inside this section.
              overridable: false
              default: false
            viewport_height:
              type: select.select
              label: Height
              description: Enabling viewport height on a section that directly follows the header will subtract the header's height from it. On short pages, a section can be expanded to fill the browser window.
              default: none
              options:
                none: None
                full: Viewport
                percent: Viewport (Minus 20%)
                section: Viewport (Minus 50%)
                expand: Expand
            padding:
              type: select.select
              label: Padding
              description: Set the vertical padding of the particle inside section. The padding is not needed if you set Height to Viewport mode.
              default: default
              options:
                default: Default
                xsmall: X-Small
                small: Small
                large: Large
                xlarge: X-Large
            vertical_alignment:
              type: select.select
              label: Vertical Alignment
              description: Align the section content vertically, if the section height is larger than the content itself.
              default: none
              options:
                none: Top
                middle: Middle
                bottom: Bottom
            parallax_text_color:
              type: select.select
              label: Inverse Color
              description: Set light or dark color mode for text, buttons and controls.
              default: default
              options:
                default: Default
                light: Light
                dark: Dark
    copyright:
      type: separator.note
      class: alert alert-info
      content: 'JL Popover <strong>Version: 1.0.5</strong> Copyright (C) <a href="https://joomlead.com/" target="_blank">JoomLead</a>.'
